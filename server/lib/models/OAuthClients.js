var db = require( __dirname + '/DB' );

var OAuthClientsSchema = db.Schema ({
  clientId: { type: String },
  clientSecret: { type: String },
  redirectUris: { type: Array }
});

module.exports = db.model('OAuthClients', OAuthClientsSchema);