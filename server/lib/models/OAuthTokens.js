var db = require( __dirname + '/DB' );

var OAuthTokensSchema = db.Schema ({
  accessToken: { type: String },
  accessTokenExpiresAt: { type: Date },
  client : { type: Object },  // `client` and `user` are required in multiple places, for example `getAccessToken()`
  clientId: { type: String },
  refreshToken: { type: String },
  refreshTokenExpiresAt: { type: Date },
  user : { type: Object },
  userId: { type: String },
});

module.exports = db.model('OAuthTokens', OAuthTokensSchema);