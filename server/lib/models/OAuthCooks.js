var db = require( __dirname + '/DB' );

var OAuthCooksSchema = db.Schema ({
  firstname: { type: String },
  lastname: { type: String },
  email: { type: String, default: '' },
  password: { type: String }
}, {
  timestamps: true
});

module.exports = db.model('OAuthCooks', OAuthCooksSchema);