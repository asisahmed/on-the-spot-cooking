var db = require(__dirname + '/DB');

var Dishes = db.Schema({
  name: { type: String },
  cusine: { type: String },
  cook: { type: String },
  images: { type: Array },
  rating: { type: Number, 'default': 0 },
  orderCount: { type: Number, 'default': 0 },
  hourlyRate: { type: Number, 'default': 0 },
  timetocook: { type: String },
  ingredients: { type: Array }
}, {
    timestamps: true
  });

module.exports = db.model('Dishes', Dishes);