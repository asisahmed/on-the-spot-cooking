var db = require( __dirname + '/DB' );

var OAuthUsersSchema = db.Schema ({
  name: { type: String },
  //lastname: { type: String },
  email: { type: String, default: '' },
  username: { type: String, default: '' },
  password: { type: String },
  displayPicturePath: { type: String },
  dishesOrdered: { type: Array, default: [] }
}, {
  timestamps: true
});

module.exports = db.model('OAuthUsers', OAuthUsersSchema);