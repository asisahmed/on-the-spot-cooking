/**
 * This module is responsible for configuring how the server
 * handles requests for static content, images, javascript
 * and html
 */

contentServer = module.exports = {};

/**
 * Setup takes an express application server and configures
 * it to handle JavaScript requests.
 * @param {String} app module
 * @param {String} express module
 *
 * @returns {undefined}
 */
contentServer.setup = function contentServer ( app, express, config ) {

  app.use( '/reset-password', express.static( __dirname + '/../views/forget-password.html' ));

  app.use( '/terms-and-conditions', express.static( __dirname + '/../views/terms-and-conditions.html' ));

  app.use( '/uploads', express.static(  __dirname + '/../pictures' ));
  
  app.use( '/assets', express.static( __dirname + '/../assets' ));

};
