Dishes = module.exports = {};

Dishes.setup = function dishesSetup(app) {

  var Dishes = require('./../models/Dishes.js');
  var OAuthUsers = require('./../models/OAuthUsers.js');

  /**
   * @api {get}  /api/dishes/popular  Dishes.
   */
  app.get('/api/dishes/popular', function dishesApiCallback(req, res) {

    Dishes.find({}, function getDataFromDBCallback(error, dishesData) {
      dishesData.forEach(element => {
        element.images.push('http://localhost:3000/api/image/' + Math.round(Math.random() * (17 - 0) + 0));
      });
      res.status(200).jsonp(dishesData);
    }).limit(10).sort('orderCount');
  });

  /**
   * @api {get}  /api/dishes/recent  Dishes.
   */
  app.get('/api/dishes/recent', function dishesApiCallback(req, res) {

    Dishes.find({}, function getDataFromDBCallback(error, dishesData) {
      dishesData.forEach(element => {
        element.images.push('http://localhost:3000/api/image/' + Math.round(Math.random() * (17 - 0) + 0));
      });
      res.status(200).jsonp(dishesData);
    }).limit(10).sort('createdAt');
  });

  /**
   * @api {post}  /api/dish/create  Create Dish.
   */
  app.post('/api/dish/create', function dishAddApiCallback(req, res) {

    if (req.body.userId) {

      var dish = new Dishes();

      dish.name = req.body.name;
      dish.cusine = req.body.cusine;
      dish.cook = req.body.cook;
      dish.rating = req.body.rating;
      dish.orderCount = 0;
      dish.hourlyRate = req.body.hourlyRate;
      dish.ingredients = req.body.ingredients;
      dish.utensils = req.body.utensils;
      dish.instructions = req.body.instructions;
      dish.timetocook = req.body.timetocook;
      //dish.images = req.body.images;

      dish.save(function dishSave(err, newDish) {
        console.log(err, newDish);
        res.status(200).jsonp(newDish);
      });

    } else {
      res.status(500).jsonp('UserId missing');
    }

  });

  /**
     * @api {get}  /api/image/:imageId  Get image of Users.
     * @apiName getImage
     * @apiGroup Image
     *
     * @apiParam {String} imageId to get image for.
     *
     * @apiSuccess (Success) {file} Image.
     */
  app.get('/api/image/:imageId', function imageCallback(req, res) {

    var fs = require('fs');
    var path = require('path');

    var imageId = req.params.imageId;

    //var imageUrl = __dirname + '/../uploads/' + imageId + '/' + imageId + '.png';
    var imageUrl = __dirname + '/../assets/1/' + imageId + '.jpg';
    console.log(imageUrl);
    // Check if file exists
    if (fs.existsSync(imageUrl)) {
      res.sendFile(path.resolve(imageUrl));
    } else { // Default picture
      var link = __dirname + '/../assets/1/1.jpg';
      res.sendFile(path.resolve(link));
    }
  });

  /**
   * @api {get}  /api/dishes/:id/view View Dish.
   */
  app.get('/api/dishes/:id/view', app.oauth.authenticate(), function viewDishApiCallback(req, res) {

    Dishes.findOne({ _id: req.params.id }, function getDataFromDBCallback(error, dishData) {
      dishData.images.push('http://localhost:3000/api/image/' + Math.round(Math.random() * (17 - 0) + 0));
      res.status(200).jsonp(dishData);
    });
  });

  /**
   * @api {post}  /api/dishes/:id/order Order Dish.
   */
  app.post('/api/dishes/:id/order', function orderDishApiCallback(req, res) {

    OAuthUsers.findOne({ _id: req.body.userId }, function getDataFromDBCallback(error, userData) {
      userData.dishesOrdered.push(req.params.id);
      userData.save(function userSave(err, newUser) {

        Dishes.findOne({ _id: req.params.id }, function getDataFromDBCallback(error, dishData) {

          dishData.orderCount++;
          dishData.save(function userSave(err, dish) {
            res.status(200).jsonp('Updated Dish and User');
          });

        });
      });
    });
  });

}