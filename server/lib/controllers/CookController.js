var passwordHash = require('password-hash');

Cook = module.exports = {};

Cook.setup = function cookSetup(app) {

  var OAuthCooks = require('./../models/OAuthCooks.js' );

  /**
   * @api {post}  /api/cook/signup  SignUp Cook.
   */
  app.post('/api/cook/signup', function signUpApiCallback(req, res) {

    OAuthCooks.findOne({ email: req.body.email }, function getDataFromDBCallback(error, cookData) {

      // Is this a new cook?
      if (cookData === null || cookData.length === 0) {

        var cook = new OAuthCooks();

        cook.firstName = req.body.firstName;
        cook.lastname = req.body.lastName;
        cook.email = req.body.email;
        cook.cookname = req.body.email;
        cook.password = passwordHash.generate(req.body.password);

        cook.save(function cookSave(err, newCook) {
          res.send('Cook Created');
        });

      } else {
        res.send('Cook Already Registered!');
      }
    });
  });

  /**
   * @api {post}  /api/cook/dish/add  Add Dish.
   */
  app.post('/api/cook/dish/add', function addDishApiCallback(req, res) {

    OAuthCooks.findOne({ email: req.body.email }, function getDataFromDBCallback(error, cookData) {

      // Is this a new cook?
      if (cookData === null || cookData.length === 0) {

        var cook = new OAuthCooks();

        cook.firstName = req.body.firstName;
        cook.lastname = req.body.lastName;
        cook.email = req.body.email;
        cook.cookname = req.body.email;
        cook.password = passwordHash.generate(req.body.password);

        cook.save(function cookSave(err, newCook) {
          res.send('Cook Created');
        });

      } else {
        res.send('Cook Already Registered!');
      }
    });
  });
}