User = module.exports = {};

User.setup = function userSetup(app, multipartMiddleware) {

  var OAuthUsers = require('./../models/OAuthUsers.js');
  var passwordHash = require('password-hash');

  /**
   * @api {post}  /  Index.
   */
  app.get('/', function (req, res) {
    res.send('Hello World!')
  });

  /**
   * @api {post}  /api/user/signup  SignUp User.
   */
  app.post('/api/user/signup', function signUpApiCallback(req, res) {

    if (req.body.email && req.body.password) {

      OAuthUsers.findOne({ email: req.body.email }, function getDataFromDBCallback(error, userData) {

        // Is this a new user?
        if (userData === null || userData.length === 0) {

          var user = new OAuthUsers();

          user.name = req.body.name;
          //user.lastname = req.body.lastName;
          user.email = req.body.email;
          user.username = req.body.email;
          user.password = req.body.password;
          user.type = req.body.type;
          //user.password = passwordHash.generate(req.body.password);

          // if (req.files && req.files.displayPicture && req.files.displayPicture.size > 0) {

          //   var temporaryPath = req.files.displayPicture.path;
          //   var folderPath = config.displayPictureUploadPath + user._id;
          //   var targetPath = folderPath + "/" + req.files.displayPicture.name;
          //   var file = req.files.displayPicture;

          //   uploadFile(file, temporaryPath, folderPath, targetPath, fs, HTTP, config, function saveDisplayPictureCallback(response) {

          //     user.displayPicturePath = '/uploads/' + user._id + "/" + req.files.displayPicture.name;

          //   });

          // } else {
          //   user.displayPicturePath = config.defaultPicture;
          // }

          // Save User
          user.save(function userSave(err, newUser) {
            res.status(200).jsonp('User Created');
          });

          function uploadFile(file, temporaryPath, folderPath, targetPath, fs, HTTP, config, callback) {

            var responseJSON = {};

            // if file is uploaded
            if (file && file.size > 0) {

              try {
                // Create parent folder if it does not exist
                if (!fs.existsSync(folderPath)) {
                  fs.mkdirSync(folderPath);
                }

                // move the file from the temporary location to the target folder location
                var is = fs.createReadStream(temporaryPath);
                var os = fs.createWriteStream(targetPath);

                is.pipe(os);

                is.on('end', function () {
                  // remove file
                  fs.unlinkSync(temporaryPath);
                });

                responseJSON.status = HTTP.OK_TEXT;
              } catch (err) {
                responseJSON.status = HTTP.FAIL_TEXT;
                responseJSON.message = err;
              }

              callback(responseJSON);
            }

          }

        } else {
          res.status(500).jsonp('User Already Registered!');
        }

      });
    } else {
      res.status(500).jsonp('Incomplete Data');
    }
  });

  /**
   * @api {get}  /api/user Login
   */
  app.get('/api/profile', app.oauth.authenticate(), function UserApiCallback(req, res) {

    if (req.query.email) {
      OAuthUsers.findOne({ email: req.query.email }, function getDataFromDBCallback(error, user) {

        var data = {};

        data.id = user._id;
        data.email = user.email;
        data.username = user.username;
        data.name = user.name;
        data.type = user.type;
console.log(data, user);
        //res.status(200).jsonp(data);
        res.status(200).jsonp(user);
      });
    } else {
      res.status(500).jsonp('Fail');
    }

  });

  /**
   * @api {get}  /api/user/upload  upload profile picture.
   * @apiName upload
   * @apiGroup User
   *
   * @apiParam {String} Token OAuth access token.
   *
   * @apiSuccess (Success) {JSON} status Success or Failure.
   */
  app.post('/api/user/upload', multipartMiddleware, app.oauth.authenticate(), function (req, res) {
    //logger.info('Inside get /api/user/upload');
    //console.log(req.files, req.files.meriPictureHai);
    if (req.files && req.files.meriPictureHai && req.files.meriPictureHai.size > 0) {

      var responseJSON = {};
      var me = this;

      // var temporaryPath = req.files.meriPictureHai.path;
      // var folderPath = '/src/assets/' + user._id;
      // var targetPath = folderPath + "/" + user._id + '.png';
      // var file = req.files.meriPictureHai;

      var temporaryPath = req.files.meriPictureHai.path;
      var folderPath = 'lib/assets/1' ;
      var targetPath = folderPath + "/1" + '.png';
      var file = req.files.meriPictureHai;

      uploadFile(file, temporaryPath, folderPath, targetPath, function saveDisplayPictureCallback(response) {
        //var displayPicturePath = '/api/image/' + user._id;
        var displayPicturePath = '/api/image/userid';
        //console.log(displayPicturePath);
        res.status(200).jsonp(displayPicturePath);
        //user.save(function userSave(error, userData) {

          // if (!error) {
          //   var data = { displayPicturePath: user.displayPicturePath };
          //   responseJSON.status = 'OK';
          //   responseJSON.message = 'Success';
          //   responseJSON.data = data;
          //   callback(responseJSON);
          // } else {
          //   console.log('Image upload went wrong');
          //   responseJSON.status = 'Fail';
          //   responseJSON.message = JSON.stringify(err);
          //   callback(responseJSON);
          // }
       // });
      });

    }

    function uploadFile ( file, temporaryPath, folderPath, targetPath, callback ) {

      var fs = require('fs');
      var responseJSON = {};
      var me = this;
      // if file is uploaded
      if ( file && file.size > 0 ) {
    
        try {
          // Create parent folder if it does not exist
          if (!fs.existsSync(folderPath)) {
            fs.mkdirSync(folderPath);
          }
    
          // move the file from the temporary location to the target folder location
          var is = fs.createReadStream(temporaryPath);
          var os = fs.createWriteStream(targetPath);
    
          is.pipe(os);
    
          is.on('end', function() {
            // remove file
            fs.unlinkSync(temporaryPath);
          });
    
          responseJSON.status = 'OK';
        } catch ( err ) {
          console.log(err);
        }
    
        callback(responseJSON);
      }
    }
  });

  
}
