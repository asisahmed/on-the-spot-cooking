authenticate = module.exports = {};
var util = require('util');

authenticate.setup = function (app) {

  // Post token.
  app.post('/api/oauth/token', app.oauth.token());

  // Get authorization.
  app.get('/oauth/authorize', function (req, res) {
    // Redirect anonymous users to login page.
    if (!req.app.locals.user) {
      return res.redirect(util.format('/login?redirect=%s&client_id=%s&redirect_uri=%s', req.path, req.query.client_id, req.query.redirect_uri));
    }

    return render('authorize', {
      client_id: req.query.client_id,
      redirect_uri: req.query.redirect_uri
    });
  });

  // Post authorization.
  app.post('/oauth/authorize', function (req, res) {
    // Redirect anonymous users to login page.
    if (!req.app.locals.user) {
      return res.redirect(util.format('/login?client_id=%s&redirect_uri=%s', req.query.client_id, req.query.redirect_uri));
    }

    return app.oauth.authorize();
  });

  // Get login.
  app.get('/login', function (req) {
    return render('login', {
      redirect: req.query.redirect,
      client_id: req.query.client_id,
      redirect_uri: req.query.redirect_uri
    });
  });

  // Post login.
  // @TODO: Insert your own login mechanism.
  app.post('/login', function (req, res) {
    if (req.body.email !== 'thom@nightworld.com') {
      return render('login', {
        redirect: req.body.redirect,
        client_id: req.body.client_id,
        redirect_uri: req.body.redirect_uri
      });
    }

    // Successful logins should send the user back to /oauth/authorize.
    var path = req.body.redirect || '/home';

    return res.redirect(util.format('/%s?client_id=%s&redirect_uri=%s', path, req.query.client_id, req.query.redirect_uri));
  });

  // Get secret.
  // Will require a valid access_token.
  app.get('/secret', app.oauth.authenticate(), function (req, res) {
    res.send('Secret area');
  });

  // Does not require an access_token.
  app.get('/public', function (req, res) {
    res.send('Public area');
  });

  app.get('/Test', app.oauth.authenticate(), function (req, res) {
    res.send('Test!')
  })
  
}