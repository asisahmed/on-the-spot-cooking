@author: Muhammad Asis Ahmed

1. Create mongo db 'DbForOAuth2v3'

2. Create collection 'oauthclients' and insert
{
    "clientId": <CLIENT_ID>,
    "clientSecret": <CLIENT_SECRET>,
    "redirectUri": "/yourOauthRedirectUri",
    "grants": [
        "password",
        "authorization_code",
        "refresh_token"
    ]
}

3. Use /api/use/signup to signup
{
    email: test@test.com
    firstName: testF
    lastName: testL
    password: 12312312
}

4. use /oauth/token to get token 
{
    username: test@test.com
    password: 12312312
    client_id: <CLIENT_ID>
    client_secret: <CLIENT_SECRET>
    grant_type: password
}

5. Hit /Test api and pass token in header as 
{
    Authorization: Bearer <TOKEN>
}