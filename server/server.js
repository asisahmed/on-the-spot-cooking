/**
 * Module dependencies.
 */
var bodyParser = require('body-parser');
var express = require('express');
var oauthServer = require('express-oauth-server');
var fs = require('fs');

// Create an Express application.
var app = express();

// Add body parser.
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static('views'));

var multipart = require('connect-multiparty');

var option = {
  maxFieldsSize: 104857600 // Image Size : 100 * 1024 * 1024
};

app.use(multipart(option));

var multipartMiddleware = multipart();

// Support for cross domain.
app.all('*', function callbackAccessControl (req, res, next) {
  // res.header('Access-Control-Allow-Origin', 'http:localhost:4200');
  // res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  // res.header('Access-Control-Allow-Headers', 'Content-Type');
  // res.header('Access-Control-Allow-Headers', 'Authorization');
  // next();

  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Allow-Origin", req.headers.origin);
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  res.header(
    "Access-Control-Allow-Headers",
    "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Authorization"
  );
  if ("OPTIONS" == req.method) {
    res.sendStatus(200);
  } else {
    next();
  }
});

// Add OAuth server.
app.oauth = new oauthServer({
  model: require(__dirname + '/lib/models/OAuthModel')
});

// Setup static content
var contentServer = require( __dirname + '/lib/providers/StaticContentServer' );
contentServer.setup( app, express );

// Setup controllers
var oauthController = require( __dirname + '/lib/controllers/OAuth' );
oauthController.setup( app );

var userController = require( __dirname + '/lib/controllers/UserController' );
userController.setup( app, multipartMiddleware );

var dishController = require( __dirname + '/lib/controllers/DishesController' );
dishController.setup( app );

// Start listening for requests.
app.listen(3000);
console.info( 'Listening on port 3000');