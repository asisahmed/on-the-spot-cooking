import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../../shared/api/api.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  form: FormGroup;
  fileToUpload: File = null;
  fileList: FileList;

  constructor(private formBuilder: FormBuilder, private http: HttpClient, private apiService: ApiService) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: [null, Validators.required],
      image: [null],
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]],
      address: this.formBuilder.group({
        street: [null, Validators.required],
        street2: [null],
        zipCode: [null, Validators.required],
        city: [null, Validators.required],
        state: [null, Validators.required],
        country: [null, Validators.required]
      })
    });
  }

  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  onSubmit() {
    if (this.form.valid) {
      console.log('form submitted', this.form.value);
      this.apiService.post('/user/signup', this.form.value).then(() => {
        console.log('done');
      });
    } else {
      // validate all form fields
    }
  }

  reset() {
    this.form.reset();
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  uploadPicture = (event) => {
    // this.fileList = event.srcElement.files;

    // let self = this;
    // if (self.fileList && self.fileList.length > 0) {

    //   let file: File = self.fileList[0];

    //   console.log(file, '/api/user/upload', file.name);

    //   this.apiService.uploadFile(file, '/user/upload', 'meriPictureHai').then(function(res) {
    //     setTimeout(function() {
    //       var path = res.data.displayCarPicturePath + '?' + new Date().getTime();
    //       console.log(path);
    //     }.bind(this), 500);
    //   })
    //     .catch(function(error) {
    //       console.log(error);
    //     });
    // }

  }
}

export class UserModel {
  email: string;
  password: string;
  number: string;
  name: string;
  cook: boolean = false;
}
