import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupCookComponent } from './signup-cook.component';

describe('SignupCookComponent', () => {
  let component: SignupCookComponent;
  let fixture: ComponentFixture<SignupCookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupCookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupCookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
