import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../../shared/api/api.service';
import { environment } from '../../../environments/environment';
import {PlatformLocation } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;

  constructor(private formBuilder: FormBuilder, private apiService: ApiService, private http: HttpClient, private platformLocation: PlatformLocation) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, Validators.required],
    });
  }

  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  onSubmit() {
    if (this.form.valid) {

      const body = new HttpParams()
        .set('username', this.form.value.email)
        .set('password', this.form.value.password)
        .set('client_id', 'test')
        .set('client_secret', 'test')
        .set('grant_type', 'password');

      this.http.post(environment.apiUrl + '/oauth/token', body.toString(), {
        headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
      }).subscribe((res: any) => {

        localStorage.setItem('currentUser', JSON.stringify({ token: res.access_token }));

        this.apiService.get('/profile?email=' + this.form.value.email).then((response: any) => {
          console.log(response);
          localStorage.setItem('currentUser', JSON.stringify({ token: res.access_token, email: response.email, name: response.name, type: response.type, userId: response._id }));
          window.location.href = (this.platformLocation as any).location.origin + '/main/view';
        });
      });
    }
  }

}