import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DevExtremeModule } from 'devextreme-angular';
import { SharedModule } from '../shared/shared.module';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { UserRoutingModule } from './user-routing.module';
import { SignupCookComponent } from './signup-cook/signup-cook.component';


@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    DevExtremeModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule
  ],
  declarations: [LoginComponent, SignupComponent, SignupCookComponent]
})
export class UserModule { }
