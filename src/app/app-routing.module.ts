import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TestComponent } from './test/test.component';

const routes: Routes = [
  { path: '', redirectTo: '/main/view', pathMatch: 'full' },
  {
    path: '',
    children: [
      {
        path: 'user',
        loadChildren: 'app/user/user.module#UserModule'
      },
      {
        path: 'main',
        loadChildren: 'app/main/main.module#MainModule'
      },
      {
        path: 'test',
        component: TestComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
