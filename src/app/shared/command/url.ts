// import { Reflect } from 'reflect-metadata';

export function Url(value: string) {
    return function (target: Object) {
        (<any>target).URL = value;
    }
}