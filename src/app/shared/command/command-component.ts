import { EventEmitter, Input, SimpleChanges, Injector, Inject } from "@angular/core";
import * as lodash from 'lodash';
import { ApiService } from "../api/api.service";

export class CommandComponent {

    api: ApiService;

    constructor(@Inject(Injector) injector: Injector) {
        this.api = injector.get(ApiService);
    }


    loading: boolean;

    ngOnInit(): void {
console.log('here');
        this.paramsChanged.subscribe(this.resolve);

        this.emitChanges(this.initialChanges);
    }

    //  Url
    public get url(): string {

        //  Get the template for the url from the controller's decorator
        var urlTemplate = (<any>this.constructor).URL;

        //  Replace tokes in the url template with values from the controller's params
        let url = lodash.template(urlTemplate, { interpolate: /{([\s\S]+?)}/g })(this.params);

        return url;
    }

    $cmd: any = {};

    @Input()
    params: any;
    paramsChanged: EventEmitter<any> = new EventEmitter<any>();

    resolved: EventEmitter<any> = new EventEmitter<any>();

    protected resolve: (params: any) => Promise<any> = () => {
        this.loading = true;
        let result = this.api
            .get(this.url, {
                params: this.params
            })
            .then(result => {
                this.$cmd = result;
                this.resolved.emit(this.$cmd);
            });
        result.then(() => this.loading = false)
            .catch((error) => {
                this.loading = false;
                console.error("THere was an error executing the command", error);
            });
        return result;
    }

    execute: () => Promise<any> = () => {

        this.loading = true;
        
        let result = this
            .api
            .post(this.url, this.$cmd);
      
        result.then(() => this.loading = false)
            .catch((error) => {
                this.loading = false;
                console.error("THere was an error executing the command", error);
            });

        return result;
    }

    /**
     * Stores the initial set of changes set by the parent component
     */
    initialChanges: SimpleChanges;

    ngOnChanges(changes: SimpleChanges) {

        if (lodash.isNil(this.initialChanges)) {
            this.initialChanges = changes;
        }
        else {
            this.emitChanges(changes);
        }

    }

    /**
     * Emits a set of changes using Event Emitters defined on this component using a naming convention
     * @param changes The changes to emit
     */
    emitChanges(changes: SimpleChanges) {

        lodash.forIn(changes, (change, key) => {

            if (this[key + "Changed"]) {
                this[key + "Changed"].emit(change);
            }

        });

    }
}
