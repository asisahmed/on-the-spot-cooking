import { HttpClient, HttpParams, HttpParameterCodec, HttpHeaders } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { merge } from 'lodash';
import { environment } from '../../../environments/environment';
import { HttpParamsOptions } from '@angular/common/http/src/params';
import { Observable } from 'rxjs';

@Injectable()
export class ApiService {

  constructor(@Inject(HttpClient) private httpClient: HttpClient) { }

  public get: (path: string, options?: any) => Promise<any> = async (path: string, options?: any) => {

    var currentUser = JSON.parse(localStorage.getItem('currentUser'));

    options = options || {};

    merge(options, { withCredentials: true })

    // options.params = new HttpParams({
    //   encoder: new UriComponentEncoder(),
    //   fromObject: options.params,

    // });

    // let headers = new HttpHeaders();
    // headers = headers.append("Authorization", 'Bearer ' + currentUser.token);
    // headers = headers.append("Content-Type", "application/x-www-form-urlencoded");

    // const httpParams: HttpParamsOptions = { fromObject: options } as HttpParamsOptions;

    // const test = { params: new HttpParams(httpParams), headers: headers };

    // console.log(test);

    // const headers = new HttpHeaders({
    //   'Content-Type': 'application/json',
    //   'Authorization': 'Bearer ' + currentUser.token
    // })
    // const httpParams: HttpParamsOptions = { fromObject: options } as HttpParamsOptions;
    // const test = { params: new HttpParams(httpParams), headers: headers };

    return this.httpClient.get(environment.apiUrl + path)
      .toPromise()
      .catch(error => console.log(error));

  }

  public post: (path: string, body: any) => Promise<any> = async (path: string, body: any) => {
    
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let contentHeader = new HttpHeaders();
    if(currentUser) {
      contentHeader.append('Accept', 'application/json');
      contentHeader.append('Content-Type', 'application/json');
      contentHeader.append('Authorization', 'Bearer ' + currentUser.token);
    }

    return this.httpClient.post(environment.apiUrl + path, body, { headers: contentHeader })
      .toPromise()
      .catch(error => console.log(error));
  }

  /**
  * upload file to server
  * @function uploadFile
  *
  * @param {File} file file to be post
  * @param {string} url url of of api
  * @param {string} keyName key name for the file
  * @memberOf ApiService
  * @returns {Promise} return Promise.
  **/
  public uploadFile(file: File, url: string, keyName: string): Promise<any> {
    return new Promise((resolve, reject) => {
      let xhr: XMLHttpRequest = new XMLHttpRequest();
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      };

      var currentUser = JSON.parse(localStorage.getItem('currentUser'));

      xhr.open('POST', environment.apiUrl + url, true);
      xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      xhr.setRequestHeader('Authorization', 'Bearer ' + currentUser.token);
      let formData = new FormData();
      formData.append(keyName, file, file.name);
      xhr.send(formData);
    });
  }
}

class UriComponentEncoder implements HttpParameterCodec {
  encodeKey(key: string): string {
    return encodeURIComponent(key);
  }

  encodeValue(value: string): string {
    return encodeURIComponent(value);
  }

  decodeKey(key: string): string {
    return decodeURIComponent(key);
  }

  decodeValue(value: string): string {
    return decodeURIComponent(value);
  }
}