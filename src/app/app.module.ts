import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { AppRoutingModule } from './app-routing.module';
import { ApiModule } from './shared/api/api.module';
import { SharedModule } from './shared/shared.module';
import { DevExtremeModule } from 'devextreme-angular';
import { NguCarouselModule } from '@ngu/carousel';
import { TestComponent } from './test/test.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    TestComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DevExtremeModule,
    ApiModule,
    SharedModule.forRoot(),
	NguCarouselModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
