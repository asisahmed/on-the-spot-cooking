import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditComponent } from './edit/edit.component';
import { DeleteComponent } from './delete/delete.component';
import { CreateComponent } from './create/create.component';
import { OrderComponent } from './order/order.component';

const routes: Routes = [ 
  {
    path: '',
    children: [
      {
        path: 'create',
        component: CreateComponent
      },
      {
        path: ':id/edit',
        component: EditComponent
      },
      {
        path: ':id/delete',
        component: DeleteComponent
      },
      {
        path: ':id/order',
        component: OrderComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DishRoutingModule { }
