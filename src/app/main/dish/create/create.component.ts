import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../../../shared/api/api.service';
import { PlatformLocation } from '@angular/common';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  form: FormGroup;

  cusineTypes: any = [
    "Chinese",
    "Mexican",
    "Italian",
    "Japanese",
    "Greek",
    "French",
    "Thai",
    "Spanish",
    "Indian",
    "Mediterranean"
  ];

  ingredientsList: any = [];

  constructor(private formBuilder: FormBuilder, private apiService: ApiService, private platformLocation: PlatformLocation) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: [null, [Validators.required]],
      cusine: [null, Validators.required],
      ingredients: [null, Validators.required],
      //images: [null, Validators.required],
      timeToCook: [null, Validators.required],
      utensils: [null, Validators.required],
      instructions: [null, Validators.required],
      hourlyRate: [null],
      image: [null]
    });
  }

  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  onSubmit() {
    if (this.form.valid) {

      console.log(this.form.value);

      var currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.form.value.userId = currentUser.userId;

      this.apiService.post('/dish/create', this.form.value).then((response: any) => {
        console.log('response');
        window.location.href = (this.platformLocation as any).location.origin + '/main/view';
      });

    }
  }

  uploadPicture = () => {

  }

  // onEnterKey = (value) => {
  //   this.ingredientsList.push(value);
  // }

}
