import { Component, Injectable, Injector, OnInit } from '@angular/core';
import { ApiService } from '../../../shared/api/api.service';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { PlatformLocation } from '@angular/common';

@Component({
	selector: 'app-order',
	templateUrl: './order.component.html',
	styleUrls: ['./order.component.scss']
})

export class OrderComponent implements OnInit {

	dishId: any;
	param: any;
	url; any;
	$cmd: any;
	user: any;
	form: FormGroup;

	showRequestForm: boolean = false;

	constructor(private apiService: ApiService, private route: ActivatedRoute, private formBuilder: FormBuilder, private platformLocation: PlatformLocation) {
		this.route
			.params
			.subscribe(params => {
				this.dishId = params['id'];
				this.url = '/dishes/' + this.dishId + '/view';
			});
	}

	ngOnInit() {

		this.user = JSON.parse(localStorage.getItem('currentUser'));

		this.apiService.get(this.url).then((res) => {
			this.$cmd = res;
		});

		this.form = this.formBuilder.group({
			timeNeeded: [null, [Validators.required]],
			peopleCount: [null, Validators.required],
		});
	}

	isFieldValid(field: string) {
		return !this.form.get(field).valid && this.form.get(field).touched;
	}

	displayFieldCss(field: string) {
		return {
			'has-error': this.isFieldValid(field),
			'has-feedback': this.isFieldValid(field)
		};
	}

	orderDish = () => {
		console.log('this.user.userId', this.user.userId);
		this.apiService.post('/dishes/' + this.dishId + '/order', { userId: this.user.userId }).then((res) => {
			console.log('Dish ordered', res);
		});
	}

	onSubmit = () => {
		// Request chef api call and email
		window.location.href = (this.platformLocation as any).location.origin + '/main/view';
	}

}
