import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DishRoutingModule } from './dish-routing.module';
import { EditComponent } from './edit/edit.component';
import { DeleteComponent } from './delete/delete.component';
import { QueryComponent } from './query/query.component';
import { CreateComponent } from './create/create.component';
import { DevExtremeModule } from 'devextreme-angular';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OrderComponent } from './order/order.component';

@NgModule({
  imports: [
    CommonModule,
    DishRoutingModule,
    SharedModule,
    DevExtremeModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  declarations: [EditComponent, DeleteComponent, QueryComponent, CreateComponent, OrderComponent]
})
export class DishModule { }
