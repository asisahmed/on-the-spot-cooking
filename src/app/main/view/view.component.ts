import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import * as $ from 'jquery';
import { NguCarouselConfig, NguCarousel } from '@ngu/carousel';
import 'hammerjs';
import { ApiService } from '../../shared/api/api.service';
import { map } from 'lodash';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  name = 'Angular';
  slideNo = 0;
  withAnim = true;
  resetAnim = true;
  @ViewChild('myCarousel') myCarousel: any;

  // public carouselTile: NguCarouselConfig = {
  //   grid: { xs: 1, sm: 1, md: 3, lg: 3, all: 0 },
  //   slide: 1,
  //   speed: 250,
  //   point: {
  //     visible: true
  //   },
  //   load: 2,
  //   velocity: 0,
  //   touch: true,
  //   easing: 'cubic-bezier(0, 0, 0.2, 1)'
  // };
  carouselTile: NguCarouselConfig = {
    grid: { xs: 1, sm: 2, md: 4, lg: 4, all: 0 },
    load: 3,
    interval: {timing: 3000, initialDelay: 1000},
    loop: true,
    touch: true,
    velocity: 0.2
  }
  popularDishes: any;
  recentDishes: any;

  constructor(private apiService: ApiService, private changeDetector: ChangeDetectorRef, private router: Router) {}

  ngOnInit() {

    this.apiService.get('/dishes/popular').then((res) => this.popularDishes = res);
    this.apiService.get('/dishes/recent').then((res) => this.recentDishes = res);

  }

  ngDoCheck() {
		this.changeDetector.detectChanges();
	}

  ngAfterViewInit() {
    this.changeDetector.detectChanges();
  }

  reset() {
    this.myCarousel.reset(!this.resetAnim);
  }

  moveTo(slide) {
    this.myCarousel.moveTo(slide, !this.withAnim);
  }

  routeTo(event) {
    console.log(event);
    this.router.navigate(['main/dish/', event._id, 'order']);
  }
  
}
