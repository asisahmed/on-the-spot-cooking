import { Component, OnInit, ViewChild } from '@angular/core';
import { PlatformLocation } from '@angular/common';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  constructor(private platformLocation: PlatformLocation) { }
  user: any;

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
  }

  logout = () => {
    localStorage.clear();
    window.location.href = (this.platformLocation as any).location.origin + '/main/view';
  }

}
